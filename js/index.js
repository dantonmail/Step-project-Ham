"use strict";

let tabControl = function () {
  let tab = document.querySelector(".tabs");
  let tabContent = document.querySelector(".tabs-content");
  let tabName;
  if (tab) {
    tab.addEventListener("click", (e) => {
      if (e.target.classList.contains("tabs-title")) {
        document.querySelector(".tabs-title.active").classList.remove("active");
        e.target.classList.add("active");
        tabName = e.target.getAttribute("data-number");
        selectTabContent(tabName);
      }

      function selectTabContent(tabName) {
        document.querySelector(".tabs-panel.active").classList.remove("active");
        tabContent.children[tabName].classList.add("active");
      }
    });
  }
};

tabControl();

let findCategory = function () {
  let tab = document.querySelector(".tabs-work");
  let tabContent = document.querySelectorAll(".tabs-panel-work");
  let tabName;
  if (tab) {
    tab.addEventListener("click", (e) => {
      if (e.target.classList.contains("tabs-title-work")) {
        document
          .querySelector(".tabs-title-work.active")
          .classList.remove("active");
        e.target.classList.add("active");
        tabName = e.target.getAttribute("data-name");
        selectTabContent(tabName);
      }
      function selectTabContent(tabName) {
        tabContent.forEach((item) => {
          item.classList.contains(tabName)
            ? item.classList.add("active")
            : item.classList.remove("active");
        });
      }

      function addImage() {
        let btnLoadMore = document.querySelector(".button-load-more");

        let dch;
        let dn;

        btnLoadMore.addEventListener("click", () => {
          btnLoadMore.remove("active");
          tabContent.forEach((item) => {
            if (item.hasAttribute("data-classhidden")) {
              dch = item.getAttribute("data-classhidden");
              dn = item.getAttribute("data-name");
            }
            if (dn === tabName) {
              item.classList.add(`${dch.slice(0, 3)}`);
              item.classList.add(`${dch.slice(4, 10)}`);
              item.classList.add(`${dch.slice(11)}`);
            }

            if (item.hasAttribute("data-classhidden") && tabName === "all") {
              item.classList.add(`${dch.slice(0, 3)}`);
              item.classList.add(`${dch.slice(4, 10)}`);
              item.classList.add(`${dch.slice(11)}`);
            }
          });
        });
      }

      addImage();
    });
  }
};

findCategory();

let changeBlock = function () {
  let tabsContentWork = document.querySelectorAll(".tabs-panel-work");

  let atributeName;
  let evt = (e) => {
    let elemImg = e.target;

    if (e.type == "mouseover" && elemImg.classList.contains("work-image-img")) {
      elemImg.classList.remove("active");
      atributeName = e.currentTarget.getAttribute("data-name");
      elemImg.insertAdjacentHTML(
        "afterend",
        `<div class="creative-design">
          <div class="container-icon-design">
            <div class="icon">
              <a href="#">
              <svg class="icon-svg" width="43" height="42" viewBox="0 0 43 42" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect x="1" y="1" width="41" height="40" rx="20" stroke="#18CFAB"/>
              </svg>
              <div class="comb-shape1"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
                </svg></a>
                </div>
            </div>
            <div class="icon" >
              <a href="#">
              <svg class="icon-svg"  width="43" height="42" viewBox="0 0 43 42" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect x="1" y="1" width="41" height="40" rx="20" stroke="#18CFAB"/>
              </svg>
              <div class="comb-shape2"><svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="12" height="11" fill="white"/>
                </svg></a>
              </div>
            </div>
          </div>
        <div class="icon-arrow"><svg width="34" height="38" viewBox="0 0 34 38" fill="none" xmlns="http://www.w3.org/2000/svg">
          <g filter="url(#filter0_d)">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M19.8551 20.9922L28.4722 17.8915L5.3125 5L12.0643 30.8705L17.1205 23.1553L23.174 31L25.9087 28.8368L19.8551 20.9922Z" fill="#363B3E"/>
          <path d="M20.6704 21.2302L28.6415 18.362L29.6764 17.9896L28.7154 17.4546L5.55568 4.56312L4.53319 3.99397L4.82871 5.12626L11.5805 30.9967L11.865 32.0868L12.4825 31.1445L17.1537 24.0168L22.7782 31.3055L23.0873 31.7061L23.4842 31.3921L26.2189 29.229L26.6063 28.9225L26.3045 28.5314L20.6704 21.2302Z" stroke="white" stroke-opacity="0.8"/>
          </g>
          <defs>
          <filter id="filter0_d" x="0.753876" y="0.987915" width="33.1268" height="36.3151" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
          <feFlood flood-opacity="0" result="BackgroundImageFix"/>
          <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
          <feOffset dy="1"/>
          <feGaussianBlur stdDeviation="1.5"/>
          <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
          <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
          </filter>
          </defs>
          </svg>
          </div>
        <span class="creative-design-top"> creative design</span>
        
        <span class="creative-design-bottom">${atributeName}</span>
      </div>
      `
      );

      function outside(e, x, y) {
        let rect = e.getBoundingClientRect();
        return (
          x < rect.left || y < rect.top || x > rect.right || y > rect.bottom
        );
      }

      let elemDiv = elemImg.nextSibling;
      let elemLi = elemImg.parentNode;

      let eventOut = (e) => {
        if (e.type == "mouseout" && outside(elemLi, e.x, e.y)) {
          elemDiv.remove();
          elemImg.classList.add("active");
          elemLi.removeEventListener("mousout", eventOut);
        }
      };
      elemLi.addEventListener("mouseout", eventOut);
    }
  };

  tabsContentWork.forEach((item) => {
    if (item.classList.contains("tabs-panel-work")) {
      item.addEventListener("mouseover", evt);
    }
  });
};

changeBlock();

$(".slider-big").slick({
  slidesToShow: 1,
  arrows: true,
  dots: true,
  fade: true,
  speed: 1000,
  easing: "ease",
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});
